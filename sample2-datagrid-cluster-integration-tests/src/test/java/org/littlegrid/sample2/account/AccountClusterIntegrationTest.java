package org.littlegrid.sample2.account;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;
import org.junit.Test;
import org.littlegrid.sample2.datagrid.testsupport.AbstractClusterIntegrationTest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Account cluster integration tests.
 */
public class AccountClusterIntegrationTest extends AbstractClusterIntegrationTest {
    @Test
    public void test() {
        final NamedCache cache = CacheFactory.getCache("Account");

        assertThat(cache.size(), is(0));
    }
}

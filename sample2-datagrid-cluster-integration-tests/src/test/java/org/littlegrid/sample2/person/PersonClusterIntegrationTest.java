package org.littlegrid.sample2.person;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;
import org.junit.Test;
import org.littlegrid.sample2.datagrid.testsupport.AbstractClusterIntegrationTest;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Person cluster integration tests.
 */
public class PersonClusterIntegrationTest extends AbstractClusterIntegrationTest {
    @Test
    public void test() {
        final NamedCache cache = CacheFactory.getCache("Person");

        assertThat(cache.size(), is(0));

    }
}
